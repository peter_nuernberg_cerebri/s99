This repository contains the problems deiscussed at http://aperiodic.net/phil/scala/s-99/ and some solutions to these problems.

# Basic instructions

Clone this repository. If you have `sbt` installed, test things out by running `sbt test` in the root of the checked out code.
  
Start with `P01` and work your way up to `P99`. For each problem:

1. Start by reading the trait that defines it. For example, for `P01`, read the source in `com.cerebri.S99.P01` in `src/main`. The trait will describe the problem, provide an example, and give you a signature.

1. See if you can understand the signature, especially if it contains type parameters.

1. Read the test.  For example, for `P01`, read the test in `com.cerebri.S99.P01Spec` in `src/test`. See if you can understand what's being asserted in each test.
_The problem definitions are underspecified -- you need to read the tests to build a correct solution that passes all the tests._

1. Fill in your solution to the problem in the appropriate file in the `my` package.  For example, for `P01`, fill in your solution in `com.cerebri.S99.solutions.my.MyP01` in `src/main`.

1. Either run your test directly, or remove the `@Ignore` annotation from `com.cerebri.S99.solutions.my.MyP01Spec` in `src/test` and run all the tests (e.g., by typing `sbt test` in a shell in the root).

# Stuck?

There are many solutions provided here.  Browse some of them for inspiration. Basically speaking, the following packages have the following types of solutions:

* `simple`: These are relatively straightforward solutions that tend not to "cheat" and use builtins. Probably best to start with.

* `builtin`: These definitely cheat and use builtins wherever possible. Best for learning what's in the standard library.

* `scalaz`: Small modifications to the `simple` solutions using some scalaz sugar and types.

* `folders`: Uses `fold` where possible to solve problems. Only for FP geeks!

* `reducers`: Uses `reduce` where possible to solve problems. Only for FP geeks!

# Advanced instructions

Mastered the above?  No problem.  There's plenty more to do!

## Write your own tests

You can write your own tests.  If you tag them with `com.cerebri.S99.solutions.my.ExtraTag`, you can run (only) them in an sbt scope called `extra`, by running `sbt extra:test`.  Pretty neat, eh?  Read `project\Build.scala` to see why this works!

## Suggest new problems!

Feel free to write new problems and submit them in a PR.  Way more likely to be merged if you also provide good tests and at least oe solution.

