/* S99 problem set.
 */
package com.cerebri.S99

/** P06 (*) Find out whether a list is a palindrome.
  * Example:
  * {{{
  * scala> isPalindrome(List(1, 2, 3, 2, 1))
  * res0: Boolean = true
  * }}}
  */
trait P06 {

  def isPalindrome(as: List[_]): Boolean
}
