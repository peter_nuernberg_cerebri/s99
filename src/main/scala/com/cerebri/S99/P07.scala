/* S99 problem set.
 */
package com.cerebri.S99

/** P07 (**) Flatten a nested list structure.
  * Example:
  * {{{
  * scala> flatten(List(List(1, 1), 2, List(3, List(5, 8))))
  * res0: List[Any] = List(1, 1, 2, 3, 5, 8)
  * }}}
  */
trait P07 {

  def flatten(as: List[_]): List[Any]
}
