/* S99 problem set.
 */
package com.cerebri.S99

/** P01 (*) Find the last element of a list.
  * Example:
  * {{{
  * scala> last(List(1, 1, 2, 3, 5, 8))
  * res0: Int = 8
  * }}}
  */
trait P01 {

  def last[A](as: List[A]): A
}
