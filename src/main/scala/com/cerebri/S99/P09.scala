/* S99 problem set.
 */
package com.cerebri.S99

/** P09 (**) Pack consecutive duplicates of list elements into sublists.
  * If a list contains repeated elements they should be placed in separate sublists.
  * Example:
  * {{{
  * scala> pack(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
  * res0: List[List[Symbol]] = List(List('a, 'a, 'a, 'a), List('b), List('c, 'c), List('a, 'a), List('d), List('e, 'e, 'e, 'e))
  * }}}
  *
  * List(List('c, 'c), List('b), List('a, 'a, 'a, 'a), )
  */
trait P09 {

  def pack[A](as: List[A]): List[List[A]]
}
