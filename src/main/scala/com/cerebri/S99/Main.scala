package com.cerebri.S99

/**
  * Created by peternuernberg on 6/9/16.
  */
object Main {
  def main(args: Array[String]): Unit = {
    val nums = (1 to 100)
    val words = nums.map(i => Word.words(i).map(_.mkString).getOrElse(i.toString))
    println(words.mkString(","))
  }
}

sealed trait Word {
  def accept(i: Int): Boolean
}
case object Fizz extends Word {
  override def accept(i: Int) = i % 3 == 0
  override val toString = "Fizz"
}
case object Buzz extends Word {
  override def accept(i: Int) = i % 5 == 0
  override val toString = "Buzz"
}
object Word {
  def instances = List(Fizz, Buzz)
  def words(i: Int): Option[List[Word]] = {
    instances.filter(_.accept(i)) match {
      case Nil => None
      case l => Some(l)
    }
  }
}
