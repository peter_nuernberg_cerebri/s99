/* S99 problem set.
 */
package com.cerebri.S99

/** P05 (*) Reverse a list.
  * Example:
  * {{{
  * scala> reverse(List(1, 1, 2, 3, 5, 8))
  * res0: List[Int] = List(8, 5, 3, 2, 1, 1)
  * }}}
  */
trait P05 {

  def reverse[A](as: List[A]): List[A]
}
