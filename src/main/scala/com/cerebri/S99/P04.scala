/* S99 problem set.
 */
package com.cerebri.S99

/** P04 (*) Find the number of elements of a list.
  * Example:
  * {{{
  * scala> length(List(1, 1, 2, 3, 5, 8))
  * res0: Int = 6
  * }}}
  */
trait P04 {

  def length(as: List[_]): Int
}
