package com.cerebri.S99.solutions.reducers

import com.cerebri.S99.P01

/** A solution to P01 using <code>reduce</code>.
  */
object P01Reducer extends P01 {

  override def last[A](as: List[A]): A =
    as.reduceRightOption[A] { case (elem, acc) => acc }
      .getOrElse(throw new Exception("no last of an empty list"))
}
