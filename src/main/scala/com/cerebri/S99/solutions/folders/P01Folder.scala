package com.cerebri.S99.solutions.folders

import com.cerebri.S99.P01

import scalaz._
import Scalaz._

/** A solution to P01 using <code>fold</code>.
  */
object P01Folder extends P01 {

  override def last[A](as: List[A]): A =
    as.foldLeft(new Exception("no last of an empty list").left[A]) { case (_, elem) => elem.right }
      .valueOr(err => throw err)
}
