/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.P10

/** Fill in your solution to P10 here!
  */
object MyP10 extends P10 {

  override def encode[A](as: List[A]): List[(Int, A)] = ??? // replace the "???" with your solution
}
