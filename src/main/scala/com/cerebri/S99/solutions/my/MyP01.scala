/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.P01

/** Fill in your solution to P01 here!
  */
object MyP01 extends P01 {

  override def last[A](as: List[A]): A = ??? // replace the "???" with your solution
}
