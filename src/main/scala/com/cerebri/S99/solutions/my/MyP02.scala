/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.P02

/** Fill in your solution to P02 here!
  */
object MyP02 extends P02 {

  override def penultimate[A](as: List[A]): A = ??? // replace the "???" with your solution
}
