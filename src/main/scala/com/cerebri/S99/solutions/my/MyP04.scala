/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.P04

/** Fill in your solution to P04 here!
  */
object MyP04 extends P04 {

  override def length(as: List[_]): Int = ??? // replace the "???" with your solution
}
