/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.P03

/** Fill in your solution to P03 here!
  */
object MyP03 extends P03 {

  override def nth[A](index: Int, as: List[A]): A = ??? // replace the "???" with your solution
}
