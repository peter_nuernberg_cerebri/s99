/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.P06

/** Fill in your solution to P06 here!
  */
object MyP06 extends P06 {

  override def isPalindrome(as: List[_]): Boolean = ??? // replace the "???" with your solution
}
