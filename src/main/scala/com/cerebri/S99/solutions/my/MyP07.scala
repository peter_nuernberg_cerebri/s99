/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.P07

/** Fill in your solution to P07 here!
  */
object MyP07 extends P07 {

  override def flatten(as: List[_]): List[Any] = ??? // replace the "???" with your solution
}
