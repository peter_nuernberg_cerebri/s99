/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.P09

/** Fill in your solution to P09 here!
  */
object MyP09 extends P09 {

  override def pack[A](as: List[A]): List[List[A]] = ??? // replace the "???" with your solution
}
