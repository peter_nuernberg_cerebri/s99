/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.P05

/** Fill in your solution to P05 here!
  */
object MyP05 extends P05 {

  override def reverse[A](as: List[A]): List[A] = ??? // replace the "???" with your solution
}
