/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.P08

/** Fill in your solution to P08 here!
  */
object MyP08 extends P08 {

  override def compress[A](as: List[A]): List[A] = ??? // replace the "???" with your solution
}
