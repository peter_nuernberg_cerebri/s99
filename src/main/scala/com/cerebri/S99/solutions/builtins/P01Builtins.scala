package com.cerebri.S99.solutions.builtins

import com.cerebri.S99.P01

/** A solution to P01 using builtins.
  */
object P01Builtins extends P01 {

  override def last[A](as: List[A]): A = as.last
}
