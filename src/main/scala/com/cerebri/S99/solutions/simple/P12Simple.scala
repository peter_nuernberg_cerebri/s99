package com.cerebri.S99.solutions.simple

import com.cerebri.S99.P12

/** A simple solution to P12.
  */
object P12Simple extends P12 {

  override def decode[A](enc: List[(Int, A)]): List[A] = {
    def aux(rest: List[(Int, A)], acc: List[A] = Nil): List[A] = {
      rest match {
        case Nil => acc
        case (0, _) :: t => aux(t, acc)
        case (n, a) :: t => aux((n - 1, a) :: t, a :: acc)
      }
    }
    aux(enc.reverse)
  }
}
