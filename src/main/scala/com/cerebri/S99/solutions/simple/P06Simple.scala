/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.P06

/** A simple solution to P06.
  */
object P06Simple extends P06 {

  override def isPalindrome(as: List[_]): Boolean = {
    def aux(orig: List[_], rev: List[_]): Boolean =
      (orig, rev) match {
        case (Nil, Nil) => true
        case (hl :: tl, hr :: tr) =>
          if (hl != hr)
            false
          else
            aux(tl, tr)
        case _ => throw new Exception("this should never happen! (one list shorter than the other)")
      }
    aux(as, as.reverse)
  }
}
