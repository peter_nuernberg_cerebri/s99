/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.P05

/** A simple solution to P05.
  */
object P05Simple extends P05 {

  override def reverse[A](as: List[A]): List[A] = {
    def aux(rest: List[A], acc: List[A] = Nil): List[A] =
      rest match {
        case Nil => acc
        case h :: t => aux(t, h :: acc)
      }
    aux(as)
  }
}
