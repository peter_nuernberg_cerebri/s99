package com.cerebri.S99.solutions.simple

import com.cerebri.S99.P11

/** A simple solution to P11.
  */
object P11Simple extends P11 {

  override def encodeModified[A](as: List[A]): List[Any] =
    P10Simple.encode(as).map {
      case (1, a) => a
      case t@_ => t
    }
}
