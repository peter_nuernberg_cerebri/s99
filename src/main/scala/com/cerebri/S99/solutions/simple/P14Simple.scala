package com.cerebri.S99.solutions.simple

import com.cerebri.S99.P14

/** A simple solution to P14.
  */
object P14Simple extends P14 {

  override def duplicate[A](as: List[A]): List[A] = {
    def aux(rest: List[A], acc: List[A] = Nil): List[A] =
      rest match {
        case Nil => acc.reverse
        case h :: t => aux(t, h :: h :: acc)
      }
    aux(as)
  }
}
