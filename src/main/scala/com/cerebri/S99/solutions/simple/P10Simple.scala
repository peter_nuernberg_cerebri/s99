/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.P10

/** A simple solution to P10.
  */
object P10Simple extends P10 {

  override def encode[A](as: List[A]): List[(Int, A)] =
    P09Simple.pack(as) match {
      case List(Nil) => List.empty[(Int, A)]
      case pack@_ => pack.map { l => (l.length, l.headOption.getOrElse(throw new Exception("cannot encode an empty sublist"))) }
    }
}
