package com.cerebri.S99.solutions.simple

import com.cerebri.S99.P13

/** A simple solution to P13.
  */
object P13Simple extends P13 {

  override def encodeDirect[A](as: List[A]): List[(Int, A)] = {
    def aux(rest: List[A], acc: List[(Int, A)] = Nil): List[(Int, A)] = {
      (rest, acc) match {
        case (Nil, _) => acc.reverse
        case (hr :: tr, Nil) => aux(tr, List((1, hr)))
        case (hr :: tr, (na, ea) :: ta) =>
          val newAcc = if (hr == ea) (na + 1, ea) :: ta else (1, hr) :: acc
          aux(tr, newAcc)
      }
    }
    aux(as)
  }
}
