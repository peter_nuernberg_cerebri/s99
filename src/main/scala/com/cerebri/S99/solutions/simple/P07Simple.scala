/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.P07

/** A simple solution to P07.
  */
object P07Simple extends P07 {

  override def flatten(as: List[_]): List[Any] = {
    def aux(rest: List[_], acc: List[_] = Nil): List[Any] =
      rest match {
        case Nil => acc.reverse
        case (h :: t) =>
          h match {
            case Nil => aux(t, acc)
            case (hi :: ti) => aux((hi :: ti) ++ t, acc)
            case _ => aux(t, h :: acc)
          }
      }
    aux(as)
  }
//  override def flatten(as: List[_]): List[Any] = {
//      as match {
//        case Nil => Nil
//        case (h :: t) =>
//          h match {
//            case Nil => flatten(t)
//            case (hi :: ti) => flatten((hi :: ti) ++ t)
//            case _ => h :: flatten(t)
//          }
//      }
//  }
}
