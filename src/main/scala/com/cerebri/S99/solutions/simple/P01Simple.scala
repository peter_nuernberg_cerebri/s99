/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.P01

/** A simple solution to P01.
  */
object P01Simple extends P01 {

  override def last[A](as: List[A]): A =
    as match {
      case Nil => throw new Exception("no last of an empty list")
      case h :: Nil => h
      case _ :: t => last(t)
    }
}
