/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.P04

/** A simple solution to P04.
  */
object P04Simple extends P04 {

  override def length(as: List[_]): Int = {
    def aux(rest: List[_], acc: Int = 0): Int = {
      rest match {
        case Nil => acc
        case _ :: t => aux(t, acc + 1)
      }
    }
    aux(as)
  }
}
