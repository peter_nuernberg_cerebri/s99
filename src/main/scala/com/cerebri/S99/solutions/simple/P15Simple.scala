package com.cerebri.S99.solutions.simple

import com.cerebri.S99.P15

/** A simple solution to P15.
  */
object P15Simple extends P15 {

  override def duplicateN[A](n: Int, as: List[A]): List[A] = {
    def aux(rest: List[A], curr: Int, acc: List[A] = Nil): List[A] =
      (rest, curr) match {
        case (Nil, _) => acc.reverse
        case (_ :: tr, 0) => aux(tr, n, acc)
        case (hr :: _, _) => aux(rest, curr - 1, hr :: acc)
      }
    aux(as, n)
  }
}
