/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.P08

/** A simple solution to P08.
  */
object P08Simple extends P08 {

  //  override def compress[A](as: List[A]): List[A] = {
  //    def aux(rest: List[A], last: Option[A] = None, acc: List[A] = Nil): List[A] =
  //      (rest, last) match {
  //        case (Nil, _) => acc.reverse
  //        case (h :: t, None) => aux(t, Some(h), h :: acc)
  //        case (h :: t, Some(l)) =>
  //          if (h == l)
  //            aux(t, Some(l), acc)
  //          else
  //            aux(t, Some(h), h :: acc)
  //      }
  //    aux(as)
  //  }

  //  override def compress[A](as: List[A]): List[A] = {
  //    def aux(rest: List[A], acc: List[A] = Nil): List[A] =
  //      rest match {
  //        case Nil => acc.reverse
  //        case h :: t => aux(t, acc.headOption.map(last => if (last == h) acc else h :: acc).getOrElse(List(h)))
  //      }
  //    aux(as)
  //  }

  override def compress[A](as: List[A]): List[A] = {
    def aux(rest: List[A], acc: List[A] = Nil): List[A] =
      rest match {
        case Nil => acc.reverse
        case h :: t =>
          acc match {
            case Nil => aux(t, List(h))
            case ha :: _ =>
              if (ha == h)
                aux(t, acc)
              else
                aux(t, h :: acc)
          }
      }
    aux(as)
  }
}
