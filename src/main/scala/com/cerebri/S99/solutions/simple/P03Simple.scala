/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.P03

/** A simple solution to P03.
  */
object P03Simple extends P03 {

  override def nth[A](index: Int, as: List[A]): A =
    (index, as) match {
      case (i, _) if (i < 0) => throw new Exception("index is negative")
      case (_, Nil) => throw new Exception("list too short")
      case (0, h :: _) => h
      case (_, _ :: t) => nth(index - 1, t)
    }
}
