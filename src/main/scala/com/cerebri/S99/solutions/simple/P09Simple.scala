/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.P09

/** A simple solution to P09.
  */
object P09Simple extends P09 {

  override def pack[A](as: List[A]): List[List[A]] = {
    def aux(rest: List[A], last: Option[A] = None, curr: List[A] = Nil, acc: List[List[A]] = Nil): List[List[A]] =
      (rest, last) match {
        case (Nil, _) => (curr :: acc).reverse
        case (h :: t, None) => aux(t, Some(h), List(h), acc)
        case (h :: t, Some(l)) =>
          if (h == l)
            aux(t, Some(l), h :: curr, acc)
          else
            aux(t, Some(h), List(h), curr :: acc)
      }
    aux(as)
  }

//  def pack2[A](as: List[A]): List[List[A]] = {
//    as.foldLeft((List.empty[List[A]], Option.empty[A], List.empty[A])) {
//      case ((acc, None, curr), elem) => (acc, Some(elem), List(elem))
//      case ((acc, Some(l), curr), elem) => if (l == elem) (acc, Some(elem), elem :: curr) else (curr :: acc, Some(elem), List(elem))
//    }._1
//  }

//  def pack3[A](as: List[A]): List[List[A]] = {
//    as.foldLeft(List.empty[List[A]]) {
//      case (Nil, elem) => List(List(elem))
//      //case (h :: t, elem) => if (h.head == elem) (elem :: h) :: t else List(elem) :: (h :: t)
//      case (acc, elem) => acc match {
//        case (h :: t) => if (h.head == elem) (elem :: h) :: t else List(elem) :: acc
//      }
//    }
//  }
}
