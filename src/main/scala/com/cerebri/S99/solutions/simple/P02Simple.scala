/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.P02

/** A simple solution to P02.
  */
object P02Simple extends P02 {

  override def penultimate[A](as: List[A]): A =
    as match {
      case Nil => throw new Exception("no penultimate of an empty list")
      case _ :: Nil => throw new Exception("no penultimate of a 1-element list")
      case p :: _ :: Nil => p
      case _ :: t => penultimate(t)
    }
}
