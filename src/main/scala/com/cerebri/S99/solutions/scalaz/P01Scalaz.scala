package com.cerebri.S99.solutions.scalaz

import com.cerebri.S99.P01
import scalaz._
import Scalaz._

/** A scalaz-based solution to P01.
  */
object P01Scalaz extends P01 {

  override def last[A](as: List[A]): A = f_last(as).valueOr(err => throw new Exception(err.toString))

  def f_last[A](as: List[A]): S99Error \/ A =
    as match {
      case Nil => EmptyList.left
      case h :: Nil => h.right
      case _ :: t => f_last(t)
    }
}
