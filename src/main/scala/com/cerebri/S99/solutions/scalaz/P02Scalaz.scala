package com.cerebri.S99.solutions.scalaz

import com.cerebri.S99.P02
import scalaz._
import Scalaz._

/** A scalaz-based solution to P02.
  */
object P02Scalaz extends P02 {

  override def penultimate[A](as: List[A]): A = f_penultimate(as).valueOr(err => throw new Exception(err.toString))

  def f_penultimate[A](as: List[A]): S99Error \/ A =
    as match {
      case Nil => EmptyList.left
      case _ :: Nil => ListTooShort.left
      case p :: _ :: Nil => p.right
      case _ :: t => f_penultimate(t)
    }
}
