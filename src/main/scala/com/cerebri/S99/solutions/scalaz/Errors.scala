package com.cerebri.S99.solutions.scalaz

/** Error objects for the scalaz-based solutions.
  */
sealed trait S99Error

sealed trait ListError extends S99Error

case object EmptyList extends ListError {
  override val toString = "unexpected empty list"
}

case object ListTooShort extends ListError {
  override val toString = "list was too short for requested operation"
}
