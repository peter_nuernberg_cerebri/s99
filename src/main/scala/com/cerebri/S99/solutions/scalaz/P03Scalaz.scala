package com.cerebri.S99.solutions.scalaz

import com.cerebri.S99.P03
import scalaz._
import Scalaz._

/** A scalaz-based solution to P03.
  */
object P03Scalaz extends P03 {

  override def nth[A](index: Int, as: List[A]): A = f_nth(index, as).valueOr(err => throw new Exception(err.toString))

  def f_nth[A](index: Int, as: List[A]): S99Error \/ A =
    (index, as) match {
      case (_, Nil) => ListTooShort.left
      case (0, h :: _) => h.right
      case (_, h :: t) => f_nth(index - 1, t)
    }

}
