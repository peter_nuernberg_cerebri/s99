/* S99 problem set.
 */
package com.cerebri.S99

/** P02 (*) Find the last but one element of a list.
  * Example:
  * {{{
  * scala> penultimate(List(1, 1, 2, 3, 5, 8))
  * res0: Int = 5
  * }}}
  */
trait P02 {

  def penultimate[A](as: List[A]): A
}
