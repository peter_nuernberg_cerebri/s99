/* S99 problem set.
 */
package com.cerebri.S99

/** Specification of P07.
  */
abstract class P07Spec extends BaseSpec[P07] {

  describe("a solution to problem 07") {

    describe("when given the example") {

      it("will return the provided output") {
        newSolutionInstance.flatten(List(List(1, 1), 2, List(3, List(5, 8)))) shouldBe List(1, 1, 2, 3, 5, 8)
      }
    }

    describe("when given an empty list") {

      it("will return an empty list") {
        newSolutionInstance.flatten(Nil) shouldBe Nil
      }
    }

    describe("when given a flat list") {

      it("will return the given list") {
        forAll(listOfIntsGen) { (list) =>
          newSolutionInstance.flatten(list) shouldBe list
        }
      }
    }
  }
}
