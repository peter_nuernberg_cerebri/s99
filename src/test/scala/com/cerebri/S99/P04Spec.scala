/* S99 problem set.
 */
package com.cerebri.S99

/** Specification of P04.
  */
abstract class P04Spec extends BaseSpec[P04] {

  describe("a solution to problem 04") {

    describe("when given the example") {

      it("will return the provided output") {
        newSolutionInstance.length(List(1, 1, 2, 3, 5, 8)) shouldBe 6
      }
    }

    describe("when given an empty list") {

      it("will return 0") {
        newSolutionInstance.length(Nil) shouldBe 0
      }
    }

    describe("when given a non-empty list") {

      it("will return the correct length") {
        forAll(smallIntGen) { (baseSize) =>
            val size = baseSize + 1 // non-zero size
            newSolutionInstance.length(List.fill(size)(0)) shouldBe size
        }
      }
    }
  }
}
