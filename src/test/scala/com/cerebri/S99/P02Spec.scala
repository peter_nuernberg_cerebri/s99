/* S99 problem set.
 */
package com.cerebri.S99

/** Specification of P02.
  */
abstract class P02Spec extends BaseSpec[P02] {

  describe("a solution to problem 02") {

    describe("when given the example") {

      it("will return the provided output") {
        newSolutionInstance.penultimate(List(1, 1, 2, 3, 5, 8)) shouldBe 5
      }
    }

    describe("when given a 2+ element list") {

      it("will return the penultimate element in the list") {
        forAll(intGen, intGen, listOfIntsGen) { (last, penultimate, list) =>
          newSolutionInstance.penultimate((last :: penultimate :: list).reverse) shouldBe penultimate
        }
      }
    }

    describe("when given a 1-element list") {

      it ("will fail") {
        forAll(intGen) { (elem) =>
          intercept[Exception] {
            newSolutionInstance.penultimate(List(elem))
          }
        }
      }
    }

    describe("when given an empty list") {

      it ("will fail") {
        intercept[Exception] {
          newSolutionInstance.penultimate(List.empty)
        }
      }
    }
  }
}
