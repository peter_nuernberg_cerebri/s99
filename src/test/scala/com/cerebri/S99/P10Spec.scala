/* S99 problem set.
 */
package com.cerebri.S99

/** Specification of P010.
  */
abstract class P10Spec extends BaseSpec[P10] {

  describe("a solution to problem 10") {

    describe("when given the example") {

      it("will return the provided output") {
        newSolutionInstance.encode(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) shouldBe
          List((4,'a), (1,'b), (2,'c), (2,'a), (1,'d), (4,'e))
      }
    }

    describe("when given an empty list") {

      it("should return a list containing an empty list") {
        newSolutionInstance.encode(Nil) shouldBe Nil
      }
    }

    describe("when given a list consisting of many copies of the same element") {

      it("will return a list containing the given multiple and element") {
        forAll(intGen, smallIntGen) { (elem, mult) =>
          val size = mult + 1
          val list = List.fill(size)(elem)
          newSolutionInstance.encode(list) shouldBe List((size, elem))
        }
      }
    }
  }
}
