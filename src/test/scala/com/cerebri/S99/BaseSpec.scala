/* S99 problem set.
 */
package com.cerebri.S99

import org.scalacheck.Gen
import org.scalacheck.Gen._
import org.scalatest._
import org.scalatest.prop.PropertyChecks

/** Base class for all specifications.
  */
abstract class BaseSpec[P] extends FunSpec with Matchers with PropertyChecks {

  def newSolutionInstance: P

  val intGen: Gen[Int] = chooseNum(Int.MinValue, Int.MaxValue)

  val smallIntGen: Gen[Int] = chooseNum(0, 1000)

  val listOfIntsGen: Gen[List[Int]] =
    for {
      size <- smallIntGen
      list <- listOfN(size, intGen)
    } yield list
}
