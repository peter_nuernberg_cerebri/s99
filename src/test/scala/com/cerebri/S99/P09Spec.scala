/* S99 problem set.
 */
package com.cerebri.S99

/** Specification of P09.
  */
abstract class P09Spec extends BaseSpec[P09] {

  describe("a solution to problem 09") {

    describe("when given the example") {

      it("will return the provided output") {
        newSolutionInstance.pack(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) shouldBe
          List(List('a, 'a, 'a, 'a), List('b), List('c, 'c), List('a, 'a), List('d), List('e, 'e, 'e, 'e))
      }
    }

    describe("when given an empty list") {

      it("will return an empty list") {
        newSolutionInstance.pack(Nil) shouldBe List(Nil)
      }
    }

    describe("when given a list consisting of many copies of the same element") {

      it("will return a list containing the given list") {
        forAll(intGen, smallIntGen) { (elem, mult) =>
          val list = List.fill(mult)(elem)
          newSolutionInstance.pack(list) shouldBe List(list)
        }
      }
    }
  }
}
