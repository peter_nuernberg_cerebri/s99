/* S99 problem set.
 */
package com.cerebri.S99

/** Specification of P06.
  */
abstract class P06Spec extends BaseSpec[P06] {

  describe("a solution to problem 06") {

    describe("when given the example") {

      it("will return the provided output") {
        newSolutionInstance.isPalindrome(List(1, 2, 3, 2, 1)) shouldBe true
      }
    }

    describe("when given an empty list") {

      it("will return true") {
        newSolutionInstance.isPalindrome(Nil) shouldBe true
      }
    }

    describe("when given a 1-element list") {

      it("will return true") {
        forAll(intGen) { (elem) =>
          newSolutionInstance.isPalindrome(List(elem)) shouldBe true
        }
      }
    }

    describe("when given a palindrome of even length") {

      it("will return true") {
        forAll(listOfIntsGen) { (list) =>
          newSolutionInstance.isPalindrome(list ++ list.reverse) shouldBe true
        }
      }
    }

    describe("when given a palindrome of odd length") {

      it("will return true") {
        forAll(listOfIntsGen, intGen) { (list, elem) =>
          newSolutionInstance.isPalindrome(list ++ List(elem) ++ list.reverse) shouldBe true
        }
      }
    }

    describe("when given a non-palindrome of even length") {

      it("will return false") {
        forAll(intGen, listOfIntsGen) { (h, t) =>
          val list = h :: t
          val newH = if (h == 0) 1 else 0
          val newList = newH :: t
          newSolutionInstance.isPalindrome(list ++ newList.reverse) shouldBe false
        }
      }
    }

    describe("when given a non-palindrome of odd length") {

      it("will return false") {
        forAll(intGen, listOfIntsGen, intGen) { (h, t, elem) =>
          val list = h :: t
          val newH = if (h == 0) 1 else 0
          val newList = newH :: t
          newSolutionInstance.isPalindrome(list ++ List(elem) ++ newList.reverse) shouldBe false
        }
      }
    }
  }
}
