/* S99 problem set.
 */
package com.cerebri.S99

/** Specification of P08.
  */
abstract class P08Spec extends BaseSpec[P08] {

  describe("a solution to problem 08") {

    describe("when given the example") {

      it("will return the provided output") {
        newSolutionInstance.compress(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) shouldBe List('a, 'b, 'c, 'a, 'd, 'e)
      }
    }

    describe("when given an empty list") {

      it("will return an empty list") {
        newSolutionInstance.compress(Nil) shouldBe Nil
      }
    }

    describe("when given a list consisting of many copies of the same element") {

      it("will return a list with just one copy of the element") {
        forAll(intGen, smallIntGen) { (elem, mult) =>
          newSolutionInstance.compress(List.fill(mult.abs + 1)(elem)) shouldBe List(elem)
        }
      }
    }
  }
}
