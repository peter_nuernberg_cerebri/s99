/* S99 problem set.
 */
package com.cerebri.S99

/** Specification of P01.
  */
abstract class P01Spec extends BaseSpec[P01] {

  describe("a solution to problem 01") {

    describe("when given the example") {

      it("will return the provided output") {
        newSolutionInstance.last(List(1, 1, 2, 3, 5, 8)) shouldBe 8
      }
    }

    describe("when given a non-empty list") {

      it("will return the last element in the list") {
        forAll(intGen, listOfIntsGen) { case (last, list) =>
          newSolutionInstance.last((last :: list).reverse) shouldBe last
        }
      }
    }

    describe("when given an empty list") {

      it ("will fail") {
        intercept[Exception] {
          newSolutionInstance.last(List.empty)
        }
      }
    }
  }
}
