package com.cerebri.S99.solutions.scalaz

import com.cerebri.S99.{P02, P02Spec}

class P02ScalazSpec extends P02Spec {

  override val newSolutionInstance: P02 = P02Scalaz
}
