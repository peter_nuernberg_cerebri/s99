package com.cerebri.S99.solutions.builtins

import com.cerebri.S99.{P01, P01Spec}

class P01BuiltinsSpec extends P01Spec {

  override val newSolutionInstance: P01 = P01Builtins
}
