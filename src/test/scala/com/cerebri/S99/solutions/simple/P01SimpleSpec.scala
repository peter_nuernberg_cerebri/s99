/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.{P01, P01Spec}

class P01SimpleSpec extends P01Spec {

  override val newSolutionInstance: P01 = P01Simple
}
