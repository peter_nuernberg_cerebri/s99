/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.{P08, P08Spec}

class P08SimpleSpec extends P08Spec {

  override val newSolutionInstance: P08 = P08Simple
}
