/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.{P03, P03Spec}

class P03SimpleSpec extends P03Spec {

  override val newSolutionInstance: P03 = P03Simple
}
