/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.{P04, P04Spec}

class P04SimpleSpec extends P04Spec {

  override val newSolutionInstance: P04 = P04Simple
}
