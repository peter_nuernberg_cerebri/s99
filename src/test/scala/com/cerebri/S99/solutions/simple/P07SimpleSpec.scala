/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.{P07, P07Spec}

class P07SimpleSpec extends P07Spec {

  override val newSolutionInstance: P07 = P07Simple
}
