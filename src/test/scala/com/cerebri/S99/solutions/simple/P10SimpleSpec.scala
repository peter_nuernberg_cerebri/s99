/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.{P10, P10Spec}

class P10SimpleSpec extends P10Spec {

  override val newSolutionInstance: P10 = P10Simple
}
