/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.{P09, P09Spec}

class P09SimpleSpec extends P09Spec {

  override val newSolutionInstance: P09 = P09Simple
}
