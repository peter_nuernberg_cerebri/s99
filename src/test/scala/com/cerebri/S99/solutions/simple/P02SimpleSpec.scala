/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.{P02, P02Spec}

class P02SimpleSpec extends P02Spec {

  override val newSolutionInstance: P02 = P02Simple
}
