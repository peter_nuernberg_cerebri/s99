/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.{P05, P05Spec}

class P05SimpleSpec extends P05Spec {

  override val newSolutionInstance: P05 = P05Simple
}
