/* S99 problem set.
 */
package com.cerebri.S99.solutions.simple

import com.cerebri.S99.{P06, P06Spec}

class P06SimpleSpec extends P06Spec {

  override val newSolutionInstance: P06 = P06Simple
}
