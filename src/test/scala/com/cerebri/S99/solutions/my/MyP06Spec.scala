/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.{P06, P06Spec}
import org.scalatest.Ignore

@Ignore class MyP06Spec extends P06Spec {

  override val newSolutionInstance: P06 = MyP06
}
