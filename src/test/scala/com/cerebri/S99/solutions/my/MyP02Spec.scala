/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.{P02, P02Spec}
import org.scalatest.Ignore

@Ignore class MyP02Spec extends P02Spec {

  override val newSolutionInstance: P02 = MyP02
}
