/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.{P01, P01Spec}
import org.scalatest.Ignore

@Ignore class MyP01Spec extends P01Spec {

  override val newSolutionInstance: P01 = MyP01
}
