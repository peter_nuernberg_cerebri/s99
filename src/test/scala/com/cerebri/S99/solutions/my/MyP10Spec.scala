/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.{P10, P10Spec}
import org.scalatest.Ignore

@Ignore class MyP10Spec extends P10Spec {

  override val newSolutionInstance: P10 = MyP10
}
