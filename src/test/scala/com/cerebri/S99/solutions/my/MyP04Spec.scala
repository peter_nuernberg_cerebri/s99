/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.{P04, P04Spec}
import org.scalatest.Ignore

@Ignore class MyP04Spec extends P04Spec {

  override val newSolutionInstance: P04 = MyP04
}
