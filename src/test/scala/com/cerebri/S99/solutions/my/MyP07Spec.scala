/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.{P07, P07Spec}
import org.scalatest.Ignore

@Ignore class MyP07Spec extends P07Spec {

  override val newSolutionInstance: P07 = MyP07
}
