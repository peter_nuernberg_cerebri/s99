/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.{P09, P09Spec}
import org.scalatest.Ignore

@Ignore class MyP09Spec extends P09Spec {

  override val newSolutionInstance: P09 = MyP09
}
