/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.{P05, P05Spec}
import org.scalatest.Ignore

@Ignore class MyP05Spec extends P05Spec {

  override val newSolutionInstance: P05 = MyP05
}
