/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.{P03, P03Spec}
import org.scalatest.Ignore

@Ignore class MyP03Spec extends P03Spec {

  override val newSolutionInstance: P03 = MyP03
}
