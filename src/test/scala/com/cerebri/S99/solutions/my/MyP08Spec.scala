/* S99 problem set.
 */
package com.cerebri.S99.solutions.my

import com.cerebri.S99.{P08, P08Spec}
import org.scalatest.Ignore

@Ignore class MyP08Spec extends P08Spec {

  override val newSolutionInstance: P08 = MyP08
}
