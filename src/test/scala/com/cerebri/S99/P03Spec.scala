/* S99 problem set.
 */
package com.cerebri.S99

/** Specification of P03.
  */
abstract class P03Spec extends BaseSpec[P03] {

  describe("a solution to problem 03") {

    describe("when given the example") {

      it("will return the provided output") {
        newSolutionInstance.nth(2, List(1, 1, 2, 3, 5, 8)) shouldBe 2
      }
    }

    describe("when given a sufficiently long list") {

      it("will return the expected number") {
        forAll(smallIntGen, smallIntGen) { (prefixLength, suffixLength) =>
          newSolutionInstance.nth(prefixLength, List.fill(prefixLength)(-1) ++ List(0) ++ List.fill(suffixLength)(1)) shouldBe 0
        }
      }
    }

    describe("when given a list that is too short") {

      it("will fail") {
        forAll(smallIntGen, smallIntGen) { (size, offset) =>
          intercept[Exception] {
            newSolutionInstance.nth(size + offset, List.fill(size)(0))
          }
        }
      }
    }

    describe("when given a negative index") {

      it("will fail") {
        forAll(smallIntGen, listOfIntsGen) { (index, list) =>
          intercept[Exception] {
            newSolutionInstance.nth(-index - 1, list)
          }
        }
      }
    }
  }
}
