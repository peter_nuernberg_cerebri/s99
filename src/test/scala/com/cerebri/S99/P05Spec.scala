/* S99 problem set.
 */
package com.cerebri.S99

/** Specification of P05.
  */
abstract class P05Spec extends BaseSpec[P05] {

  describe("a solution to problem 05") {

    describe("when given the example") {

      it("will return the provided output") {
        newSolutionInstance.reverse(List(1, 1, 2, 3, 5, 8)) shouldBe List(8, 5, 3, 2, 1, 1)
      }
    }

    describe("when given an empty list") {

      it("will return an empty list") {
        newSolutionInstance.reverse(Nil) shouldBe Nil
      }
    }

    describe("when given a non-empty list") {

      it("will return the expected result") {
        forAll(listOfIntsGen) { (list) =>
          newSolutionInstance.reverse(list) shouldBe list.reverse // cheat!
        }
      }
    }
  }
}
