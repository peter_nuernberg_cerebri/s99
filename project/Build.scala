import sbt._
import Keys._

object S99Build extends Build {

  override lazy val settings = super.settings

  lazy val ExtraTests = config("extra") extend Test

  lazy val kernel = Project(id = "S99", base = file("."), settings = settings)
    .configs(ExtraTests)
    .settings(inConfig(ExtraTests)(Defaults.testTasks): _*)
    .settings(testOptions in ExtraTests := Seq(Tests.Argument("-n", "com.cerebri.S99.solutions.my.MyTag")))
}

