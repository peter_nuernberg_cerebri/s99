name := "S99"

version := "0.0.1"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "org.scala-lang"          %   "scala-reflect"  %  "2.11.8",
  "org.scala-lang.modules"  %%  "scala-xml"      %  "1.0.4",
  "org.scalaz"              %%  "scalaz-core"    %  "7.2.3",
  "org.scalactic"           %%  "scalactic"      %  "2.2.6",
  "org.scalacheck"          %%  "scalacheck"     %  "1.12.5"     % "test",
  "org.scalatest"           %%  "scalatest"      %  "2.2.6"      % "test"
)
